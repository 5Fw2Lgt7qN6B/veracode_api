"""
Veracode Integration with Jenkins
Date: 03/Feb/2021
Department: SecDevOps
Description: Python class that uses the Veracode Dynamic Analysis REST API. Can be used as a Python module
Version: 1.0
Authors: Stefaniya Samm - stefaniya.samm@appetize.com
         Jonathan Angeles - jonathan.angeles@appetize.com

"""
from restServices.DynamicScan import DynamicScan

if __name__ == "__main__":

    # Instantiate class
    a = DynamicScan()
    a.scan_now(scan_name="connect-dc05-qa")
"""
Veracode Integration with Jenkins
Date: 03/Feb/2021
Department: SecDevOps
Description: Python class that uses the Veracode Dynamic Analysis REST API. Can be used as a Python module
Version: 1.0
Authors: Stefaniya Samm - stefaniya.samm@appetize.com
         Jonathan Angeles - jonathan.angeles@appetize.com

"""
from veracode_api_signing.plugin_requests import RequestsAuthPluginVeracodeHMAC
import sys
import requests
import logging
import json

#  Uncomment to Debug
import urllib3
urllib3.disable_warnings()
certVerify = False
proxies = {"http": "http://127.0.0.1:8080", "https": "http://127.0.0.1:8080"},

# Global Variable
APP_NAME = "DynamicAnalysis"


# Python class that uses the Veracode Dynamic Analysis REST API.
class DynamicScan:
    def __str__(self):
        return "DynamicAnalysis class that uses the Veracode Dynamic Analysis REST API."

    def __repr__(self):
        return f"{__name__}{self.__class__.__name__} This method returns a HTTP requests response "

    # Configure logging
    log = logging.getLogger(APP_NAME)
    log_format = '%(asctime)-15s %(levelname)s: %(message)s'
    logging.basicConfig(format=log_format, datefmt='%Y/%m/%d-%H:%M:%S')
    log.setLevel(logging.INFO)
    log.setLevel(logging.DEBUG)

    # Configure Headers
    headers = {
        'Content-Type': 'application/json',
        'User-Agent': 'Veracode DynamicAnalysis REST API Client'
    }

    #  get_analysis(): get analysis summary for a given scan name
    def get_analysis(self, scan_name: str):
        """Return a JSON format str """
        try:
            api_base = f"https://api.veracode.com/was/configservice/v1/analyses" + "?" + f"name={scan_name}"
            self.log.debug(f"Sending request to {api_base}")
            response = requests.request(method="get",
                                        headers=self.headers,
                                        url=api_base,
                                        # Uncomment to Debug
                                        # verify=certVerify,
                                        # proxies=proxies,
                                        auth=RequestsAuthPluginVeracodeHMAC(),

                                        )
            if response.ok:
                analysis_summary = json.dumps(response.json(), indent=4)
                # print(analysis_summary)
                return analysis_summary
            else:
                self.log.error(f"Request for scan spec failed with {response.status_code} code {response.text}")
                sys.exit(1)
        except requests.RequestException as e:
            self.log.error("Request for scan data failed.")
            print(e)
            sys.exit(1)

    # scan_now(): Update a scan's schedule to start ASAP
    def scan_now(self, scan_name: str):
        """Return a Python Requests Response Object """

        analysis_summary = self.get_analysis(scan_name=scan_name)
        try:
            #  Parse the json response to get the URL with scan_id
            url = json.loads(analysis_summary)['_embedded']['analyses'][0]['_links']['self']['href']
            self.log.info(f"Updating scan named {scan_name} to scan ASAP\nurl:{url}")
        except Exception as e:
            #  Verify if the scan name exist in Veracode Platform
            if json.loads(analysis_summary)['page']['total_elements'] == 0:
                self.log.error(
                    f"{e} Occurred during Parsing Json Response in scan_now method, Scan: {scan_name} was not "
                    f"Found")
                sys.exit(1)
        else:
            # Build schedule structure
            schedule_data_payload = {"schedule": {"now": True, "duration": {"length": 3, "unit": "DAY"}}}
            self.log.debug("New schedule data: %s", schedule_data_payload)
            # Send scan config update REST call
            parm = "?method=PATCH"
            url = url + parm
            self.log.info(f"Updating scan by sending a PUT request to: {url}")
            self.log.debug("PUT Body: " + json.dumps(schedule_data_payload, sort_keys=True, indent=4))
            response = requests.request(
                #  Uncomment to Debug
                # verify=certVerify,
                # proxies=proxies,
                method="put",
                url=url,
                auth=RequestsAuthPluginVeracodeHMAC(),
                headers=self.headers,
                json=schedule_data_payload,
            )
            #  Response Verification based on Veracode SwaggerHub
            if response.status_code == 204:
                self.log.debug("HTTP Code 204 The Dynamic Analysis is successfully updated. No response body is "
                               "returned")
            if response.status_code == 401:
                self.log.error("HTTP Code 401 You are not authorized to perform this action.")
                sys.exit(1)
            if response.status_code == 403:
                self.log.error("HTTP Code 403 Access denied. You are not authorized to make this request.")
                sys.exit(1)
            if response.status_code == 404:
                self.log.error("HTTP Code 404 Unknown Dynamic Analysis identifier. Verify the Dynamic Analysis ID and "
                               "try again.")
                sys.exit(1)
            if response.status_code == 500:
                self.log.error("HTTP Code 500 Server-side error. Please try again later.")
                sys.exit(1)
            if response.status_code == 400 and response.json():
                veracode_error_code = response.json()['_embedded']['errors'][0]['code']
                self.log.error(
                    f"HTTP Code 400 \nPossible Causes: \n[1] Invalid request. If there is no response body,your request"
                    f" contains malformed JSON. If there is a response body, \nVeracode could not validate your JSON"
                    f" payload. Verify the source attribute in each service error in the response and the associated "
                    f"code.\n[2] {veracode_error_code}")
                sys.exit(1)

            return response

    # get_audits() Returns a list of audit logs for the specified Dynamic Analysis.
    def get_audits(self, scan_name: str):
        """Return a JSON format str """
        analysis_summary = self.get_analysis(scan_name=scan_name)
        #  Parse the json response to get the URL with scan_id
        url = json.loads(analysis_summary)['_embedded']['analyses'][0]["_links"]["audits"]["href"]
        self.log.debug(f"Sending request to {url}")
        response = requests.request(
                #  Uncomment to Debug
                # verify=certVerify,
                # proxies={"http": "http://127.0.0.1:8080", "https": "http://127.0.0.1:8080"},
                method="get",
                url=url,
                auth=RequestsAuthPluginVeracodeHMAC(),
                headers=self.headers,
            )
        audit_response = json.dumps(response.json()['_embedded'], indent=2)
        return audit_response


if __name__ == "__main__":
    # Instantiate class
    a = DynamicScan()
    analysis = a.get_analysis(scan_name="connect-dc05-qa")
    print(analysis)
